# react-viber-link React component

Tiny React component that generates Viber deep link to specific user profile by number.

## Install

`yarn add react-viber-link` or `npm i react-viber-link`

## Use

```
import ViberLink from 'react-viber-link'

...
// Inside parent component
return (
  ...
  <ViberLink number="+1 (111) 111-1111">+1 (111) 111-1111 (Viber)</ViberLink>
  ...
)
...
```

## Under the hood

Viber deep links use different schema and number formats for desktops and mobile devices. This component checks the device type, transforms number accordingly and generates link with correct schema
