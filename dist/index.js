import React from 'react';
import PropTypes from 'prop-types';
import useIsDesktop from 'use-is-desktop';

function generateDesktopLink(number) {
  return `viber://chat?number=${encodeURIComponent(number.replace(/[^0-9+]/g, ''))}`;
}

function generateMobileLink(number) {
  return `viber://add?number=${encodeURIComponent(number.replace(/\D/g, ''))}`;
}

function ViberLink({
  number,
  className,
  children
}) {
  const isDesktop = useIsDesktop(false);
  return /*#__PURE__*/React.createElement("a", {
    className: className,
    href: isDesktop ? generateDesktopLink(number) : generateMobileLink(number)
  }, children);
}

ViberLink.defaultProps = {
  className: ''
};
ViberLink.propTypes = {
  number: PropTypes.string.isRequired,
  className: PropTypes.string,
  children: PropTypes.node.isRequired
};
export default ViberLink;
